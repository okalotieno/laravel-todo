$(function() {
	var init, submitTask, addPendingTask, completeTask, deleteTask;

	init = function() {
		$('[name="task-input"]').keyup(submitTask);
		$('.task-list').on('click', '.done', completeTask);
		$('.task-list').on('click', '.delete', deleteTask);
		$('.task-list').on('blur', '.description', updateDescription);
		$('.task-list').on('keypress', '.description', updateDescription);
	}

	submitTask = function(event) {
		var keyCode = event.which;
		if(keyCode == 13) {
			var taskDescription = $(event.target).val();
			$.post('/todo/task/add', { description: taskDescription }, function(data) {
				if(data['success']) {
					addPendingTask(taskDescription, data['id']);
					$(event.target).val('');
				} else {
					console.error(data);
				}
			});
		}
	}

	addPendingTask = function(descriptionText, id) {
		var task = document.createElement('li');
		task.setAttribute('data-id', id);
		task.setAttribute('class', 'task');

		var doneLink = document.createElement('a')
		doneLink.setAttribute('href', '#')
		doneLink.setAttribute('class', 'done')
		doneLink.innerHTML = '&#x2714;'

		var deleteLink = document.createElement('a')
		deleteLink.setAttribute('href', '#');
		deleteLink.setAttribute('class', 'delete');
		deleteLink.innerHTML = '&#x2717;';

		var description = document.createElement('span');
		description.setAttribute('class', 'description');
		description.innerHTML = descriptionText;

		$(task).append($(description));
		$(task).append($(doneLink));
		$(task).append($(deleteLink));
		$('#pending-task-list').prepend($(task));
	}

	completeTask = function(event) {
		event.preventDefault();
		var taskElement = $($(event.target).parent());
		var taskId = taskElement.attr('data-id');
		$.post('/todo/task/complete', { id: taskId }, function(data) {
			if(data['success']) {
				console.log(data)
				taskElement.children('.done').remove();
				taskElement.detach();
				$('#completed-task-list').prepend(taskElement);
			} else {
				console.error(data)
			}
		});
	}

	deleteTask = function(event) {
		event.preventDefault();
		var taskElement = $($(event.target).parent());
		var taskId = taskElement.attr('data-id');
		console.log(taskId);
		$.post('/todo/task/delete', { id: taskId }, function(data) {
			if(data['success']) {
				taskElement.remove();
			} else {
				console.error(data)
			}
		});
	}

	updateDescription = function(event) {
		if(event.type == 'keypress') {
			if(event.which == 13) {
				event.preventDefault();
			} else {
				return;
			}
		}
		var taskElement = $($(event.target).parent());
		var taskId = taskElement.attr('data-id');
		var description = $(event.target).html();
		console.log(taskId);
		$.post('/todo/task/update', { id: taskId, description: description }, function(data) {
			if(data['success']) {
				console.log(data);
			} else {
				console.error(data)
			}
		});
	}

	init();
});

# To setup

1. Install [composer](http://getcomposer.org)
2. Run `composer install` in the top-level directory
3. Run `php artisan migrate` in the top-level directory
4. Run `php artisan serve`

# To create a task

1. Login, or create an account. (seed account: admin:admin)
2. Enter a task description in the input field and press "Enter"

1. To mark a task as done, click the tick.
2. To delete a task, click the cross.
3. To edit a task description, click on it, make your changes and then press "Enter". Alternatively, you can click away to persist the changes.

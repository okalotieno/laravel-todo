<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/todo/', function()
{
	if(!Auth::check()) { return Redirect::to('/login/'); }
	$completedTasks = Task::where('completed', '=', true)->where('user_id', '=', Auth::id())->orderBy('updated_at', 'DESC')->get();
	$pendingTasks = Task::where('completed', '=', false)->where('user_id', '=', Auth::id())->orderBy('updated_at', 'DESC')->get();
	$isAdmin = User::find(Auth::id())->username == 'admin';
	return View::make('todo')->with('pendingTasks', $pendingTasks)->with('completedTasks', $completedTasks)->with('isAdmin', $isAdmin);
});

Route::get('/admin/', function()
{
	$isAdmin = User::find(Auth::id())->username == 'admin';
	if($isAdmin) {
		return View::make('admin')->with('tasks', Task::all())->with('body_class', 'admin');
	} else {
		return Redirect::to('/logout/');
	}
});

Route::post('/todo/task/add', 'TodoController@addTask');
Route::post('/todo/task/complete', 'TodoController@completeTask');
Route::post('/todo/task/delete', 'TodoController@deleteTask');
Route::post('/todo/task/update', 'TodoController@updateDescription');

Route::get('/signup/', function() {
	return View::make('signup');
});

Route::get('/login/', function() {
	return View::make('login');
});

Route::get('/logout/', function() {
	Auth::logout();
	return Redirect::to('/login/');
});

Route::post('/create-account/', 'UserController@createAccount');
Route::post('/login/', 'UserController@login');


<?php

class TodoController extends Controller {
	public function addTask() {
		try {
			$description = Input::get('description');
			$task = new Task;
			$task->description = $description;
			$task->completed = false;
			$task->user_id = Auth::id();
			$task->save();
			return Response::json(array('success' => true, 'id' => $task->id));
		} catch (Exception $e) {
			Log::error($e);
			return Response::json(array('success' => false), 500);
		}
	}

	public function completeTask() {
		try {
			$taskId = Input::get('id');
			$task = Task::find($taskId);
			$task->completed = true;
			$task->save();
			return Response::json(array('success' => true));
		} catch (Exception $e) {
			Log::error($e);
			return Response::json(array('success' => false), 500);
		}
	}

	public function deleteTask() {
		try {
			$taskId = Input::get('id');
			$task = Task::find($taskId);
			$task->delete();
			return Response::json(array('success' => true));
		} catch (Exception $e) {
			Log::error($e);
			return Response::json(array('success' => false), 500);
		}
	}

	public function updateDescription() {
		try {
			$id = Input::get('id');
			$description = Input::get('description');
			$task = Task::find($id);
			$task->description = $description;
			$task->save();
			return Response::json(array('success' => true));
		} catch (Exception $e) {
			Log::error($e);
			return Response::json(array('success' => false), 500);
		}
	}
}

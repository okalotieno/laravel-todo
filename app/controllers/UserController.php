<?php

class UserController extends BaseController {
	public function createAccount() {
		try {
			$username = Input::get('username');
			$password = Input::get('password');

			$user = new User;
			$user->username = $username;
			$user->password = Hash::make($password);
			$user->save();
			Auth::attempt(array(
				'username' => $username,
				'password' => $password
			));
			return Redirect::to('/todo/');
		} catch (Exception $e) {
			Log::error($e);
			return Redirect::to('/signup/');
		}
	}

	public function login() {
		try {
			$username = Input::get('username');
			$password = Input::get('password');
			Auth::attempt(array(
				'username' => $username,
				'password' => $password
			));
			return Redirect::to('/todo/');
		} catch (Exception $e) {
			Log::error($e);
			return Redirect::to('/signup/');
		}
	}
}

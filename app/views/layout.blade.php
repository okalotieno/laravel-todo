<!doctype html>
<html>
	<head>
		<title></title>
		<link href='/style/todo.css' rel='stylesheet' type='text/css'/>
	</head>
	<body class='{{{ $body_class or '' }}}'>
		<div class='wrapper'>
			@yield('content')
		</div>
		<script src='/js/jquery-2.1.1.min.js'></script>
		<script src='/js/todo.js'></script>
	</body>
</html>

@extends('layout')

@section('content')
	<input type='text' placeholder='What would you like to do today?' name='task-input'/>
	<ul id='pending-task-list' class='task-list'>
		@foreach($pendingTasks as $pendingTask)
			<li class='task' data-id='{{{ $pendingTask->id  }}}'>
				<span contenteditable='true' class='description'>
					{{{ $pendingTask->description }}}
				</span>
				<a href='#' class='done'>&#x2714;</a>
				<a href='#' class='delete'>&#x2717;</a>
			</li>
		@endforeach
	</ul>
	<ul id='completed-task-list' class='task-list'>
		@foreach($completedTasks as $completedTask)
			<li class='task' data-id='{{{ $completedTask->id  }}}'>
				<span contenteditable='true' class='description'>
					{{{ $completedTask->description }}}
				</span>
				<a href='#' class='delete'>&#x2717;</a>
			</li>
		@endforeach
	</ul>
	@if($isAdmin)
		<a href='/admin/'>Admin</a>
	@endif
		<a href='/logout/'>Logout</a>
@stop

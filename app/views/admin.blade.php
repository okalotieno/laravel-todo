@extends('layout')

@section('content')
<table>
	<thead>
		<tr>
			<th>Description</th>
			<th>User</th>
			<th>Date added</th>
			<th>Complete?</th>
		</tr>
	</thead>
	<tbody>
		@foreach($tasks as $task)
			<tr>
				<td>{{{ $task->description }}}</td>
				<td>{{{ User::find($task->user_id)['username'] }}}</td>
				<td>{{{ $task->created_at }}}</td>
				<td>{{{ ($task->completed)?'Yes':'No' }}}</td>
			</tr>
		@endforeach
	</tbody>
</table>
<a href='/todo/'>My list</a>
@stop
